/**
  FILE:        net.h
  AUTHOR:      t4
  DESCRIPTION: Callback-driven network declarations.
  LICENSE:     See LICENSE.
*/

#ifndef LIBIRCBOT_NET_H
#define LIBIRCBOT_NET_H

#include "message.h"
#include <stddef.h>

#define NET_OPEN_SOCKET_ERROR -1
#define NET_CONNECT_SOCKET_ERROR -1
#define NET_LOOP_SOCKET_ERROR -1
#define NET_CLOSE_SOCKET_ERROR -1
#define NET_SENDALL_SOCKET_ERROR -1

/**
  Creates a client socket with the supplied destination and port.
  Params: hostname - remote socket's hostname
          port     - port to comminicate over
          use_ssl  - should SSL be used
  Returns: socketfd on success
           NET_OPEN_SOCKET_ERROR on error, errno is set appropriately     
*/
int net_open_socket(char *hostname, int port, int use_ssl);


/**
  Connects a given client socket.
  Params: socketfd   - remote socket
          on_connect - callback triggered after socket has connected
  Returns: FALSE on success
           NET_CONNECT_SOCKET_ERROR on error, errno is set appropriately          
*/
int net_connect_socket(int irc_sockfd, void (*on_connect)(void));


/**
  A blocking, infinite loop that awaits data from the server.
  Messages will be sent to the supplied callback.
  Params: socketfd   - remote socket
          on_message - callback triggered when a frame is received
  Returns: FALSE on success
           NET_LOOP_SOCKET_ERROR on error, errno is set appropriately          
*/
int net_loop_socket(int irc_sockfd, 
                    void (*on_message)(struct irc_message *message));


/**
  Closes a given client socket.
  Params: socketfd   - remote socket
          on_close - callback triggered before socket has closed
  Returns: FALSE on success
           NET_CLOSE_SOCKET_ERROR on error, errno is set appropriately          
*/
int net_close_socket(int irc_sockfd, void (*on_close)(void));


/**
  Sends all data to a given client socket with IRC formatting.
  Params: socketfd   - remote socket
          buffer - data to be sent
  Returns: FALSE on success
           NET_SENDALL_SOCKET_ERROR on error, errno is set appropriately          
*/
int net_send_message(int irc_sockfd, char *message);

#endif
