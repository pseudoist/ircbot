/**
  FILE:        irc.h
  AUTHOR:      t4
  DESCRIPTION: IRC related constants, structures, and declarations.
  LICENSE:     See LICENSE.
*/

#ifndef LIBIRCBOT_IRC_H
#define LIBIRCBOT_IRC_H

#include "message.h"

/** Bot info structure */
struct irc_bot_info {
  char *hostname;
  int port;
  int ssl;
  char *nickname;
  char *ident;
  char *realname;
  void (*on_connect)(void);
  void (*on_message)(struct irc_message *message);
  void (*on_close)(void);
  int sockfd;
} irc_bot_info;


/**
  Initializes the IRC bot.
  Params: binfo - IRC bot information structure
  Returns: FALSE on success
           TRUE on error   
*/
int irc_init(struct irc_bot_info *binfo);


/**
  Executes the IRC socket loop.
  Params: binfo - IRC bot information structure
*/
void irc_run(struct irc_bot_info *binfo);


/**
  Destroys the IRC bot.
  Params: binfo - IRC bot information structure
*/
void irc_free(struct irc_bot_info *binfo);


/**
  Prints an IRC message structure to a buffer.
  Params: buffer  - buffer to print to
          message - IRC message structure to print  
*/
void irc_sprintf_message(char *buffer, struct irc_message *message);

#endif
