CC=clang
CFLAGS=-std=c99 -D_POSIX_C_SOURCE=200112L -Wall -pedantic -g `pkg-config --cflags glib-2.0`
LDFLAGS=-lssl -lcrypto `pkg-config --libs glib-2.0`
EXECNAME=bot
########################
DEPS = bot.o irc.o net.o

all: $(DEPS)
	$(CC) -o $(EXECNAME) $(DEPS) $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	rm -f *.o
