/**
  FILE:        irc.c
  AUTHOR:      t4
  DESCRIPTION: IRC related definitions.
  LICENSE:     See LICENSE.
*/

#include "irc.h"
#include "net.h"
#include <stdio.h>
#include <string.h>
#include <stddef.h>

int irc_init(struct irc_bot_info *binfo)
{

	binfo->sockfd = net_open_socket(binfo->hostname, binfo->port, binfo->ssl);
	if (binfo->sockfd != NET_OPEN_SOCKET_ERROR) {

		char nick_buffer[IRC_MESSAGE_MAX_LEN];
		char user_buffer[IRC_MESSAGE_MAX_LEN];
		sprintf(nick_buffer, "NICK %s", binfo->nickname);
		sprintf(user_buffer, "USER %s 0 * :%s", binfo->ident, binfo->realname);

    net_connect_socket(binfo->sockfd, binfo->on_connect);
    net_send_message(binfo->sockfd, nick_buffer);
    net_send_message(binfo->sockfd, user_buffer);
  }
  return 0;
}


void irc_run(struct irc_bot_info *binfo)
{
	net_loop_socket(binfo->sockfd, binfo->on_message);
}


void irc_free(struct irc_bot_info *binfo)
{
	net_close_socket(binfo->sockfd, binfo->on_close);
}


void irc_sprintf_message(char *buffer, struct irc_message *message)
{
  sprintf(buffer, "Prefix: '%s', Command: '%s', Params: '%s', Trail: '%s'",
          message->prefix, message->command, message->params, message->trail);
	return;
}
