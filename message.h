/**
  FILE:        message.h
  AUTHOR:      t4
  DESCRIPTION: IRC message constants and structures.
  LICENSE:     See LICENSE.
*/

#ifndef LIBIRCBOT_IRC_MESSAGE_H
#define LIBIRCBOT_IRC_MESSAGE_H

#define IRC_MESSAGE_MAX_LEN 512

/** IRC message structure */
struct irc_message {
  char prefix[IRC_MESSAGE_MAX_LEN];
  char command[IRC_MESSAGE_MAX_LEN];
  char params[IRC_MESSAGE_MAX_LEN];
  char trail[IRC_MESSAGE_MAX_LEN];
} irc_message;

#endif
