/**
  FILE:        bot.c
  AUTHOR:      t4
  DESCRIPTION: Entry point.
  LICENSE:     See LICENSE.
*/

#include <stdio.h>
#include "irc.h"

void on_connect(void)
{
  return;
}

void on_message(struct irc_message *message)
{
  if (message) {
    char buffer[1024];
    irc_sprintf_message(buffer, message);
    printf("%s\n", buffer);
  }
}

void on_close(void)
{
  return;
}

int main(int argc, char* argv[])
{
  struct irc_bot_info bot;
  bot.hostname = "localhost";
  bot.port = 6697;
  bot.ssl = 1;
  bot.nickname = "circbot";
  bot.ident = "ident";
  bot.realname = "realname";
  bot.on_connect = on_connect;
  bot.on_message = on_message;
  bot.on_close = on_close;

  irc_init(&bot);
  irc_run(&bot);
  irc_free(&bot);
  return 0;
}
