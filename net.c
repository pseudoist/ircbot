/**
  FILE:        net.c
  AUTHOR:      t4
  DESCRIPTION: Callback-driven network definitions.
  LICENSE:     See LICENSE.
*/

#include "net.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <glib.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#if GCC_VERSION
  #pragma gcc diagnostic ignored "-Wdeprecated-declarations"
#elif __clang__
  #pragma clang diagnostic ignored "-Wdeprecated-declarations"
#else
  /* Cannot determine compiler, deal with the SSL warnings */
#endif

#define SHORT_MAX_STR_LEN 6
#define IRC_MESSAGE_DELIMITER "\r\n"
#define IRC_MESSAGE_DELIMITER_LEN 2
#define IRC_MESSAGE_REGEX_PATTERN "^(:(?P<prefix>\\S+) )?(?P<command>\\S+)( (?!:)(?P<params>.+?))?( :(?P<trail>.+))?$"
#define IRC_MESSAGE_REGEX_GROUPS 5

#define IRC_PING_COMMAND "PING"
#define IRC_PING_COMMAND_LEN 4

static GRegex *irc_regex = NULL;
static struct addrinfo *addrinfo_result = NULL;
static char incoming_data_buffer[IRC_MESSAGE_MAX_LEN + 1];

static SSL_CTX *ssl_ctx = NULL;
static SSL *ssl = NULL;

static struct irc_message* net_parse_message(char *buffer, size_t length)
{
  GMatchInfo *match_info = NULL;
  struct irc_message *message = NULL;
  g_regex_match(irc_regex, buffer, 0, &match_info);

  if (g_match_info_matches(match_info)) {
    gchar *prefix = g_match_info_fetch_named(match_info, "prefix");
    gchar *command = g_match_info_fetch_named(match_info, "command");
    gchar *params = g_match_info_fetch_named(match_info, "params");
    gchar *trail = g_match_info_fetch_named(match_info, "trail");

    message = (struct irc_message*) g_malloc(sizeof(struct irc_message));
    memset(message, '\0', sizeof(struct irc_message));

    strcpy(message->prefix, prefix);
    strcpy(message->command, command);
    if (params) {
      strcpy(message->params, params);
    }
    if (trail) {
      strcpy(message->trail, trail);
    }
  }

  if (match_info) {
    g_match_info_free(match_info);
  }
  return message;
}


int net_open_socket(char *hostname, int port, int use_ssl)
{
  int irc_sockfd;
  char port_str[SHORT_MAX_STR_LEN];
  struct addrinfo hints;

  sprintf(port_str, "%d", port);

  /* Setup socket structures */
  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;

  /* Lookup address info */
  if (getaddrinfo(hostname, port_str, &hints, &addrinfo_result)) {
    if (addrinfo_result) {
      freeaddrinfo(addrinfo_result);
    }
    return NET_OPEN_SOCKET_ERROR;
  }

  /* Create socket */
  if ((irc_sockfd = socket(addrinfo_result->ai_family, addrinfo_result->ai_socktype,
       addrinfo_result->ai_protocol)) == -1) {
    freeaddrinfo(addrinfo_result);
    return NET_OPEN_SOCKET_ERROR;
  }

  if (use_ssl) {
    SSL_METHOD *method;
    SSL_library_init();
    OpenSSL_add_all_algorithms(); 
    SSL_load_error_strings();
    method = SSLv3_client_method();
    ssl_ctx = SSL_CTX_new(method);
    if (ssl_ctx == NULL) {
      ERR_print_errors_fp(stderr);
      return NET_OPEN_SOCKET_ERROR;
    }
  }

  return irc_sockfd;
}


int net_connect_socket(int irc_sockfd, void (*on_connect)(void))
{
  int rc = 0;

  /* Connect */
  if (connect(irc_sockfd, addrinfo_result->ai_addr,
              addrinfo_result->ai_addrlen) == -1) {
    close(irc_sockfd);
    rc = NET_CONNECT_SOCKET_ERROR;
    goto out;
  }

  if (ssl_ctx) {
    ssl = SSL_new(ssl_ctx);
    SSL_set_fd(ssl, irc_sockfd);
    if (SSL_connect(ssl) == -1) {
      rc = NET_CONNECT_SOCKET_ERROR;
      SSL_free(ssl);
      SSL_CTX_free(ssl_ctx);
      ERR_print_errors_fp(stderr);
      goto out;
    }
  }
  
  /* Sanitize buffer */
  memset(incoming_data_buffer, 0, IRC_MESSAGE_MAX_LEN);

  if (on_connect != NULL) {
     on_connect();
  }

  GError *gerror = NULL;

  if (irc_regex == NULL) {
    irc_regex = g_regex_new(IRC_MESSAGE_REGEX_PATTERN, 0, 0, &gerror);
  }

  if (gerror) {
    fprintf(stderr, "Regex Error: %s\n", gerror->message);
    g_error_free(gerror);
    rc = NET_CONNECT_SOCKET_ERROR;
    goto out;
  }

out:
  /* Cleanup */
  freeaddrinfo(addrinfo_result);
  return rc;
}


int net_loop_socket(int irc_sockfd, 
                    void (*on_message)(struct irc_message *message))
{
  static int recv_total_amnt = 0;
  int recv_amnt;
  char *start_ptr = NULL;
  char *end_ptr = NULL;
  incoming_data_buffer[IRC_MESSAGE_MAX_LEN] = '\0';

  while (1) {
    if (end_ptr == NULL && recv_total_amnt != IRC_MESSAGE_MAX_LEN) {
      if (ssl) {
        if ((recv_amnt = SSL_read(ssl, incoming_data_buffer + recv_total_amnt,
             IRC_MESSAGE_MAX_LEN - recv_total_amnt)) < 0) {
          SSL_free(ssl);
          SSL_CTX_free(ssl_ctx);
          close(irc_sockfd);
          return NET_LOOP_SOCKET_ERROR;
        }
      } else {
        if ((recv_amnt = recv(irc_sockfd, incoming_data_buffer + recv_total_amnt,
             IRC_MESSAGE_MAX_LEN - recv_total_amnt, 0)) == -1) {
          close(irc_sockfd);
          return NET_LOOP_SOCKET_ERROR;
        }
      }
      recv_total_amnt += recv_amnt;
    }

    start_ptr = incoming_data_buffer;
    while ((end_ptr = strstr(start_ptr, IRC_MESSAGE_DELIMITER)) != NULL) {
      end_ptr += IRC_MESSAGE_DELIMITER_LEN;
      int length = end_ptr - start_ptr;
      char buffer[length];
      memcpy(buffer, start_ptr, length);
      buffer[length - 2] = '\0';
      buffer[length - 1] = '\0';

      struct irc_message *message = net_parse_message(buffer, length);
      if (strncmp(message->command, IRC_PING_COMMAND,
                  IRC_PING_COMMAND_LEN) == 0) {
        char response[IRC_MESSAGE_MAX_LEN];
        sprintf(response, "%s :%s", IRC_PING_COMMAND, message->trail);
        net_send_message(irc_sockfd, response); //Implicitly handle ping
      } else if (on_message) {
        on_message(message);
      }
      g_free(message);
      start_ptr += length;
    }
    int trailing_data = IRC_MESSAGE_MAX_LEN - (start_ptr - incoming_data_buffer);
    memcpy(incoming_data_buffer, start_ptr, trailing_data + 1);
    int trailing_data_length = strlen(incoming_data_buffer);
    memset(incoming_data_buffer + trailing_data_length, '\0',
           IRC_MESSAGE_MAX_LEN - trailing_data_length);
    recv_total_amnt = trailing_data_length;
  }
  return 0;
}


int net_close_socket(int irc_sockfd, void (*on_close)(void))
{
  int rc = 0;
  if (on_close != NULL) {
    on_close(); 
  }
  if (ssl) {
    SSL_free(ssl);
    SSL_CTX_free(ssl_ctx);
  }
  rc = close(irc_sockfd);
  if (irc_regex) {
    g_regex_unref(irc_regex);
  }
  return rc;
}


int net_send_message(int irc_sockfd, char *message)
{
  char buffer[IRC_MESSAGE_MAX_LEN];
  int length = strlen(message) + IRC_MESSAGE_DELIMITER_LEN;
  strncpy(buffer, message, IRC_MESSAGE_MAX_LEN);
  memcpy(buffer + length - IRC_MESSAGE_DELIMITER_LEN, IRC_MESSAGE_DELIMITER,
         IRC_MESSAGE_DELIMITER_LEN);

  if (ssl) {
    int rc = SSL_write(ssl, buffer, length);
    if (rc < 0) {
      return NET_SENDALL_SOCKET_ERROR;
    }
  } else {
    size_t send_total_amnt = 0;
    size_t send_amnt = 0;
    while (send_amnt < length) {
      send_amnt = send(irc_sockfd, buffer + send_total_amnt, length, 0);
      if (send_amnt == -1) {
        return NET_SENDALL_SOCKET_ERROR;
      }
      send_total_amnt += send_amnt;
      length -= send_amnt;
    }
  }
  return 0;
}
